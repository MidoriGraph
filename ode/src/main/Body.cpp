/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "Body.hpp"
#include "Transform.hpp"
#include "utility.hpp"
#include "World.hpp"

namespace MidoriGraph {


void Body::initialize(const Node& owner) {
    
    Property::initialize(owner);
    
    dWorldID worldID = owner.getAncestorProperty<World>()->getWorldID();
    
    bodyID = dBodyCreate(worldID);
    
    // Set mass
    dMassSetSphereTotal(&mass, 1.0, 1.0);
    dBodySetMass(bodyID, &mass);

    // Set translation
    Vector3 translate = owner.getProperty<Transform>()->getTranslate();
    dBodySetPosition(bodyID, translate.x, translate.y, translate.z);
    
    // Set velocity
    dBodySetLinearVel(bodyID, 0, 0, 0);
    
}


void Body::setTransform() {
    
    //getTransform:
    //Vector3 translate = owner.getProperty<Transform>()->getTranslate();
    //sceneNode->setPosition(translate.x, translate.y, translate.z);
}


} //namespace MidoriGraph

