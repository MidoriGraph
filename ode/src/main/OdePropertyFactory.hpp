/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
 
#include "PropertyFactory.hpp" 

#ifndef _ODE_PROPERTY_FACTORY_HPP_
#define _ODE_PROPERTY_FACTORY_HPP_
 
namespace MidoriGraph {


class OdePropertyFactory : public PropertyFactory {
 
  public:
  
    OdePropertyFactory();
        
};


} //namespace MidoriGraph
#endif //_ODE_PROPERTY_FACTORY_HPP_

