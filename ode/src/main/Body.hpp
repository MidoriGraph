/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _BODY_HPP_
#define _BODY_HPP_

#include <string>
#include <ode/ode.h>

#include "Property.hpp"

namespace MidoriGraph {


class Body : public Property {
    
  public:
    
    Body(const Property::AttributeMap& attributes) {}
    
    /// \todo This should remove the body from the world.
    virtual ~Body() {}
    
    static const std::string name() { return "Body"; } 
    virtual const std::string getName() const { return name(); }
    virtual const InterfaceList getInterfaces() const { 
        InterfaceList interfaces = InterfaceList();
        interfaces.push_back(name());
        return interfaces;
    }  
    
    virtual void initialize(const Node& owner);
    
    void setTransform();
    
    
  private:
    
    dBodyID bodyID;
    dMass mass;
    
};


} //namespace MidoriGraph
#endif //_BODY_HPP_

