/* 
Copyright 2005, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <string>
#include <boost/test/auto_unit_test.hpp>
#include <boost/assign/list_of.hpp> // map_list_of());

#include "Node.hpp"
#include "Renderer.hpp"

using namespace MidoriGraph;
using std::string;
using boost::assign::map_list_of;
using boost::unit_test::test_suite;


