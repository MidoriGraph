#include <string>
#include <boost/test/included/prg_exec_monitor.hpp>
#include <boost/assign/list_of.hpp> // map_list_of());

#include "Node.hpp"
#include "utility.hpp" // VectorN
#include "CorePropertyFactory.hpp"
#include "OgrePropertyFactory.hpp"

#include "StopperTaskMaker.hpp"

using namespace std;
using boost::assign::map_list_of;
using boost::any;

int cpp_main(int, char* []) {

    CorePropertyFactory core;
    OgrePropertyFactory ogre;

    Node root = Node::makeRoot();
    root.addProperty(core.create("TaskManager"));
    root.addProperty(ogre.create("Renderer",
        map_list_of("resourcePath", string("../../resources/")) ));

    Node room = root.createChild();
    room.addProperty(ogre.create("Scene"));
    room.addProperty(ogre.create("Model",
          map_list_of("id", string("floor"))
                     ("filename", string("plane.mesh")) ));

    Node light1 = room.createChild();
    light1.addProperty(core.create("Transform",
        map_list_of("translate", Vector3(0,6,-2)) ));
    light1.addProperty(ogre.create("Light",
        map_list_of("id", string("light1")) ));

    Node light2 = room.createChild();
    light2.addProperty(core.create("Transform",
        map_list_of("translate", Vector3(1,6,2)) ));
    light2.addProperty(ogre.create("Light",
        map_list_of("id", string("light2")) ));

    Node box1 = room.createChild();
    box1.addProperty(core.create("Transform",
        map_list_of("translate", Vector3(3,2,0)) ));
    box1.addProperty(ogre.create("Model",
        map_list_of("id", string("box1"))
                   ("filename", string("cube.mesh")) ));

    Node box2 = room.createChild();
    box2.addProperty(core.create("Transform",
        map_list_of("translate", Vector3(-3,2,0)) ));
    box2.addProperty(ogre.create("Model",
        map_list_of("id", string("box2"))
                   ("filename", string("cube.mesh")) ));

    Node camera = room.createChild();
    camera.addProperty(core.create("Transform", map_list_of<string, any>
                   ("translate", Vector3(0,2,10)) 
                   ("rotate", Vector4(1,0,0,0)) ));
    camera.addProperty(ogre.create("View"));

    //root.addProperty(new StopperTaskMaker());
    root.getProperty<TaskManager>()->start();

    return 0;
}
