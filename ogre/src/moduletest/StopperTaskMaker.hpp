#include <boost/bind.hpp>

#include "Property.hpp"
#include "TaskManager.hpp"

using namespace MidoriGraph; 

class StopperTaskMaker : public Property {

  public:

    static const std::string name() { return "StopperTaskMaker"; }       
    virtual const std::string getName() const { return name(); }
    virtual const InterfaceList getInterfaces() const { 
        InterfaceList interfaces = InterfaceList();
        interfaces.push_front(name());
        return interfaces;
    }
  
    virtual void connect(const Node& newParent) {
        manager = newParent.getProperty<TaskManager>();
        task = manager->addTask(TaskManager::TaskFunction(
            bind(&StopperTaskMaker::execute, this, _1)));
    }
    
    virtual void disconnect(const Node& oldParent) {
        manager->removeTask(task);
    }
    
    void execute(Scalar deltaTime) {
        manager->stop();
    }
    
    TaskManager* manager;

  private:

    TaskManager::TaskHandle task;
    
};

