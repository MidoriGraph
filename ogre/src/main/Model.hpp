/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _MODEL_HPP_
#define _MODEL_HPP_

#include <string>
#include <Ogre/Ogre.h>

#include "Property.hpp"

namespace MidoriGraph {


//! Allows an entity to be rendered to screen.
/*!
Encapsulates an Ogre::SceneNode and Ogre::Entity.

Prerequisite properties:
Scene (ancestor): Ogre::SceneNode and Ogre::Entity must be created by an
Ogre::SceneManager.
Transform (owner, optional): If a location and direction other than (0,0,0) are desired, then a \c Transform must be present.

Valid attributes:
id: An identification string unique to all other \c Model properties.
filename: The file from which to load the model data from.
*/
class Model : public Property {
    
  public:
    
    Model(const Property::AttributeMap& attributes);
    //Model(std::string instanceName, std::string modelName):
    //    instanceName(instanceName), modelName(modelName) {}
    
    virtual ~Model() {}
    
    static const std::string name() { return "Model"; } 
    virtual const std::string getName() const { return name(); }
    virtual const InterfaceList getInterfaces() const { 
        InterfaceList interfaces = InterfaceList();
        interfaces.push_back(name());
        return interfaces;
    }  
    
    virtual void initialize(const Node& owner);
    
    //! Synchronizes the state of this \c Property with that of a \c Transform.
    void syncTransform();
    
  private:
    
    std::string instanceName;

    std::string modelName;
    
    Ogre::SceneNode* sceneNode;
    
    Ogre::Entity* entity;
    
};


} //namespace MidoriGraph
#endif //_MODEL_HPP_

