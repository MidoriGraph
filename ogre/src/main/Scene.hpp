/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _SCENE_HPP_
#define _SCENE_HPP_

#include <string>
#include <Ogre/Ogre.h>

#include "Property.hpp"

namespace MidoriGraph {


//! Allows an entity to be an area in which things can be rendered.
/*!
Encapsulates the Ogre::SceneManager.

Prerequisite properties:
Renderer: Ogre::SceneManager is created from Ogre::Root.

Planned attributes:
type: [generic|...]
ambientLight: (r,g,b)
shadowTechnique: [stencilAdditive|...]
*/
class Scene : public Property {

  public:

    //! Constructor.
    /*! \param attributes Map of attributes to use for initialization. */
    Scene(const Property::AttributeMap& attributes){}

    //! Destructor.
    virtual ~Scene(){}
    
    //! Called once when this \c Property is added to a \c Node.
    /*! \param owner The \c Node this \c Property was just added to. */
    virtual void initialize(const Node& owner);
    
    /*! \return The name of this \c Property for introspection purposes. */
    static const std::string name() { return "Scene"; } 

    /*! \return The name of this \c Property for polymorphism introspection. */
    virtual const std::string getName() const { return name(); }

    /*! \return A list of interfaces implemented by this \c Property */
    virtual const InterfaceList getInterfaces() const { 
        InterfaceList interfaces = InterfaceList();
        interfaces.push_back(name());
        return interfaces;
    }  
    
    /*! \return the Ogre::SceneManager of this scene.
    \todo This should probably by factored out by redesign.  Passing pointers
    to the low level implementation like this smells wrong.
    */
    inline Ogre::SceneManager* getSceneMgr() { return sceneMgr; }

  private:

    Ogre::SceneManager* sceneMgr;
    
    
};

} //namespace MidoriGraph

#endif //_SCENE_HPP_

