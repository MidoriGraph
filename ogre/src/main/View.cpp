/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "View.hpp"
#include "Node.hpp"
#include "Renderer.hpp"
#include "Scene.hpp"
#include "Transform.hpp"

namespace MidoriGraph {
    
using namespace boost;

View::~View() {
    /// \todo Use autoptrs instead?
    if (frameListener) {
        delete frameListener;
    }
    
}


void View::initialize(const Node& owner) {

    using namespace Ogre;
    
    Property::initialize(owner);
    
    Renderer* renderer = owner.getAncestorProperty<Renderer>();
    Root* root = renderer->getRoot();
    RenderWindow* window = renderer->getWindow();
   
    SceneManager* sceneMgr = owner.getAncestorProperty<Scene>()->getSceneMgr();
    
    camera = sceneMgr->createCamera("Camera");
    
    syncTransform();

    camera->setNearClipDistance(0.1);
    
    // Create one viewport, entire window
    Viewport* vp = window->addViewport(camera);
    vp->setBackgroundColour(ColourValue(0,0,0));
    // Alter the camera aspect ratio to match the viewport
    camera->setAspectRatio(                            
        Real(vp->getActualWidth()) / Real(vp->getActualHeight()));
    
    // Example framelistener that handles simple input
    frameListener = new ExampleFrameListener(window, camera);
    frameListener->showDebugOverlay(false);
    root->addFrameListener(frameListener);
    
}


void View::syncTransform() {

    Vector3 translate(0,0,0);
    Vector4 rotate(0,0,0,1);

    shared_ptr<Property> transform = owner.getProperty("Transform");
    if (transform) {
        translate = dynamic_pointer_cast<Transform>(transform)->getTranslate();
        rotate = dynamic_pointer_cast<Transform>(transform)->getRotate();
    }
    
    camera->setPosition(Ogre::Vector3(translate.x, translate.y, translate.z));
    camera->setOrientation(
        Ogre::Quaternion(rotate.w, rotate.x, rotate.y, rotate.z));
        
}
} //namespace MidoriGraph
