/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
 
#include <iostream>
#include <string>
#include <boost/bind.hpp>
#include <Ogre/OgreLogManager.h>
#include <Ogre/OgreWindowEventUtilities.h>

#include "Renderer.hpp"

namespace MidoriGraph {

using namespace std;
using namespace boost;



#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
#include <CoreFoundation/CoreFoundation.h>

std::string macBundlePath() {
    
    char path[1024];
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    assert(mainBundle);
    
    CFURLRef mainBundleURL = CFBundleCopyBundleURL(mainBundle);
    assert(mainBundleURL);
    
    CFStringRef cfStringRef = 
        CFURLCopyFileSystemPath( mainBundleURL, kCFURLPOSIXPathStyle);
    assert(cfStringRef);
    
    CFStringGetCString(cfStringRef, path, 1024, kCFStringEncodingASCII);
    
    CFRelease(mainBundleURL);
    CFRelease(cfStringRef);
    
    return std::string(path);
}
#endif



Renderer::Renderer(const Property::AttributeMap& attributes) {

    
    #if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
        resourcePath = macBundlePath() + "/Contents/Resources/";
    #else
        resourcePath = getAttribute<string>(attributes, "resourcePath", "");
    #endif
    
}


Renderer::~Renderer() {
    
    //if (connection.connected()) {
    //   connection.disconnect();
    //}
    /// \todo Use autoptrs instead?
    if (root) {
        delete root;
    }
    
}


void Renderer::initialize(const Node& owner) {
    
    using namespace Ogre;
    
    Property::initialize(owner);
    

    new Ogre::LogManager();
    // Normal logging:
    Ogre::LogManager::getSingleton().createLog("OgreLog", false, true, false);
    // Prevent Ogre from usinging the console at all:
    //Ogre::LogManager::getSingleton().createLog("OgreLog", true, false, true);
    
    root = new Root(resourcePath + "plugins.cfg",
                    resourcePath + "ogre.cfg",
                    "");
    
    setupResources();
    
    if (!root->restoreConfig()) {
        //throw?
    }
    
    window = root->initialise(true, "MidoriGraph");
    
    // Set default mipmap level (NB some APIs ignore this)
    TextureManager::getSingleton().setDefaultNumMipmaps(5);
    
    // Initialise, parse scripts etc
    ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
    
    TaskManager* taskMgr;
    try {
        taskMgr = owner.getProperty<TaskManager>();
    } catch (PropertyNotFound e) {
        taskMgr = owner.getAncestorProperty<TaskManager>();
    }
    taskMgr->addTask(boost::bind(&Renderer::step, &*this, _1));
    
}


/// Method which will define the source of resources (other than current folder)
void Renderer::setupResources() {
    
    using namespace Ogre;
    
    // Load resource paths from config file
    ConfigFile cf;
    cf.load(resourcePath + "resources.cfg");
    
    // Go through all sections & settings in the file
    ConfigFile::SectionIterator seci = cf.getSectionIterator();
    
    String secName, typeName, archName;
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        ConfigFile::SettingsMultiMap *settings = seci.getNext();
        ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            ResourceGroupManager::getSingleton().addResourceLocation(
                resourcePath+archName, typeName, secName);
        }
    }
}


void Renderer::step(Scalar deltaTime) {

    try {
    
        Ogre::WindowEventUtilities::messagePump();
        
        if (!root->renderOneFrame()) {
        
            TaskManager* taskMgr = dynamic_pointer_cast<TaskManager>(
                owner.getProperty("TaskManager")).get();
            if (!taskMgr) {
                taskMgr = dynamic_pointer_cast<TaskManager>(
                    owner.getAncestorProperty("TaskManager")).get();
            }
            if (!taskMgr) {
                throw(std::string("No task manager found!"));
            }

            taskMgr->stop();

        }
    } catch(Ogre::Exception& e) {
        std::cerr << "An Ogre exception has occured: ";
        std::cerr << e.getFullDescription() << std::endl;
    }
    
}

} //namespace MidoriGraph

