/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <string>

#include "Model.hpp"
#include "Scene.hpp"
#include "Transform.hpp"

namespace MidoriGraph {

using std::string;
using namespace boost;

Model::Model(const Property::AttributeMap& attributes) {

    instanceName = getAttribute<string>(attributes, "id");
    modelName = getAttribute<string>(attributes, "filename");

}

void Model::initialize(const Node& owner) {

    using namespace Ogre;
    
    Property::initialize(owner);

    // Look for a Scene in owner or an ancestor of owner.
    // Will throw std::runtime_error("Property not found: Scene") if not
    // available in the right place.
    SceneManager* sceneMgr;  
    shared_ptr<Property> scene = owner.getProperty("Scene");
    if (scene) {
        sceneMgr = dynamic_pointer_cast<Scene>(scene).get()->getSceneMgr();
    } else {
        sceneMgr = owner.getAncestorProperty<Scene>()->getSceneMgr();
    }
    
    /// \todo search for the first scenenode before using getRootSceneNode.
    sceneNode = sceneMgr->getRootSceneNode()
                ->createChildSceneNode(instanceName);

    entity = sceneMgr->createEntity(instanceName, modelName);
    entity->setCastShadows(true);
    sceneNode->attachObject(entity);
    
    syncTransform();
}


void Model::syncTransform() {

    Vector3 translate(0,0,0);

    shared_ptr<Property> transform = owner.getProperty("Transform");
    if (transform) {
        translate = dynamic_pointer_cast<Transform>(transform)->getTranslate();
    }
    
    sceneNode->setPosition(translate.x, translate.y, translate.z);
        
}

} //namespace MidoriGraph
