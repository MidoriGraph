/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _RENDERER_HPP_
#define _RENDERER_HPP_

#include <string>
#include <Ogre/Ogre.h>

#include "Property.hpp"
#include "TaskManager.hpp"

namespace MidoriGraph {


#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
// This function will locate the path to our application on OS X,
// unlike windows you can not rely on the curent working directory
// for locating your configuration files and resources.
std::string macBundlePath();
#endif


//! Makes an entity the root for all rendering tasks.
/*!
Encapsulates the Ogre::Root and other singletons such  as the resource manager.

Valid attributes:
resourcePath: A string indicating where to the root resource directory is
located.  A path relative to the executable should be used.
Ignored on OGRE_PLATFORM_APPLE because the "/Contents/Resources/"
subdirectory of the application bundle path is always used.
*/
class Renderer : public Property {

  public:

    //! Constructor.
    /*! \param attributes Map of attributes to use for initialization. */
    Renderer(const Property::AttributeMap& attributes);

    //! Destructor.
    virtual ~Renderer();

    //! Called once when this \c Property is added to a \c Node.
    /*! \param owner The \c Node this \c Property was just added to. */
    virtual void initialize(const Node& owner);
    
    /*! \return The name of this \c Property for introspection. */
    static const std::string name() { return "Renderer"; } 

    /*! \return The name of this \c Property for polymorphism introspection. */
    virtual const std::string getName() const { return name(); }

    /*! \return A list of interfaces implemented by this \c Property */
    virtual const InterfaceList getInterfaces() const { 
        InterfaceList interfaces = InterfaceList();
        interfaces.push_back(name());
        return interfaces;
    }  
    
    /*! \return the Ogre::Root of the application. 
    \todo This should probably by factored out by redesign.  Passing pointers
    to the low level implementation like this smells wrong.
    */
    inline Ogre::Root* getRoot() { return root; }
    
    /*! \return the main Ogre::RenderWindow of the application. 
    \todo This should probably by factored out by redesign.  Passing pointers
    to the low level implementation like this smells wrong.
    */
    inline Ogre::RenderWindow* getWindow() { return window; }

  private:

    Ogre::Root* root;
    Ogre::String resourcePath;
    Ogre::RenderWindow* window;

    void step(Scalar deltaTime);
    
    void setupResources();
    
};


} //namespace MidoriGraph
#endif //_RENDERER_HPP_

