/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
 This file is part of MidoriGraph.
 
 MidoriGraph is free software; you can redistribute it and/or modify it under the 
 terms of the GNU General Public License as published by the Free Software 
 Foundation; either version 2 of the License, or (at your option) any later
 version.
 
 MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License along with
 MidoriGraph; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef _VIEW_H_
#define _VIEW_H_

#include <Ogre/Ogre.h>

#include "ExampleFrameListener.h"
#include "Property.hpp"

namespace MidoriGraph {


//! Adds a viewpoint (camera) to an entity.
/*!
Encapsulates an Ogre::View.

Prerequisite properties:
Renderer (ancestor): Ogre::Root gives us window and viewport.
Scene (ancestor): Ogre::Camera must be created by an Ogre::SceneManager.
Transform (owner, optional): If a location and direction other than (0,0,0) are desired, then a \c Transform must be present.

Planned attributes:
nearClipDistance:
backgroundColor:
*/
class View : public Property {
    
  public:
    
    //! Constructor.
    /*! \param attributes Map of attributes to use for initialization. */
    View(const Property::AttributeMap& attributes) {}
    
    //! Destructor.
    virtual ~View();

    //! Called once when this \c Property is added to a \c Node.
    /*! \param owner The \c Node this \c Property was just added to. */
    virtual void initialize(const Node& owner);
    
    /*! \return The name of this \c Property for introspection. */
    static const std::string name() { return "View"; } 

    /*! \return The name of this \c Property for polymorphism introspection. */
    virtual const std::string getName() const { return name(); }

    /*! \return A list of interfaces implemented by this \c Property */
    virtual const InterfaceList getInterfaces() const { 
        InterfaceList interfaces = InterfaceList();
        interfaces.push_back(name());
        return interfaces;
    }  
    
    //! Synchronizes the state of this \c Property with that of a \c Transform.
    void syncTransform(); 
    
  private:
        
    ExampleFrameListener* frameListener;

    Ogre::Camera* camera;
    
};


} //namespace MidoriGraph

#endif //_VIEW_H_
