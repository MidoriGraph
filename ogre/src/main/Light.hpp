/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _LIGHT_HPP_
#define _LIGHT_HPP_

#include <string>
#include <Ogre/Ogre.h>

#include "Property.hpp"

namespace MidoriGraph {


//! Adds the ability to emit light and cast shadows on renderables.
/*!
Encapsulates an Ogre::Light.

Prerequisite properties:
Scene (ancestor): Ogre::Light must be created by an Ogre::SceneManager.
Transform (owner, optional): If a location and direction other than (0,0,0) are desired, then a \c Transform must be present.

Valid attributes:
id: An identification string unique to all other \c Light properties.

Planned attributes:
type: {point|direction|spot}
diffuseColor: Vector3(r,g,b)
specularColor: Vector3(r,g,b)
*/
class Light : public Property {
    
  public:
    
    //! Constructor.
    /*! \param attributes Map of attributes to use for initialization. */
    Light(const Property::AttributeMap& attributes);
    
    //! Destructor.
    virtual ~Light() {}

    //! Called once when this \c Property is added to a \c Node.
    /*! \param owner The \c Node this \c Property was just added to. */
    virtual void initialize(const Node& owner);

    /*! \return The name of this \c Property for introspection. */
    static const std::string name() { return "Light"; } 

    /*! \return The name of this \c Property for polymorphism introspection. */
    virtual const std::string getName() const { return name(); }

    /*! \return A list of interfaces implemented by this \c Property */
    virtual const InterfaceList getInterfaces() const { 
        InterfaceList interfaces = InterfaceList();
        interfaces.push_back(name());
        return interfaces;
    }  
    
    //! Synchronizes the state of this \c Property with that of a \c Transform.
    void syncTransform();
    
  private:

    std::string instanceName;

    Ogre::Light* light;
    
};


} //namespace MidoriGraph
#endif //_LIGHT_HPP_

