/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
 
#include <iostream>

#include "Scene.hpp"
#include "Renderer.hpp"

namespace MidoriGraph {

using namespace boost;
    
    
void Scene::initialize(const Node& owner) {
    
    using namespace Ogre;
    
    Property::initialize(owner);
    
    Renderer* renderer = dynamic_pointer_cast<Renderer>(
        owner.getAncestorProperty("Renderer")).get();
    Root* root = renderer->getRoot();
    
    /// \todo Dynamic scenemanager name from constructor arguments.
    sceneMgr = root->createSceneManager(ST_GENERIC, "SceneManager");
    sceneMgr->setAmbientLight( ColourValue( 0.1, 0.1, 0.1 ) );
    sceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
    
}


} //namespace MidoriGraph
