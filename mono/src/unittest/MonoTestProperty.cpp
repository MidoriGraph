#include <string>
#include <boost/test/auto_unit_test.hpp>

extern "C" {
    #include <mono/metadata/assembly.h>
}

#include "Node.hpp"
#include "MonoScriptManager.hpp"
#include "MonoScript.hpp"
#include "MonoTestProperty.hpp"

using namespace std;

class MonoTestProperty_testFramework {

  public:
    
    MonoTestProperty_testFramework() {
        
        root = Node::makeRoot();
        root.addProperty(new MonoScriptManager());

        script = root.createChild();
        Property::AttributeMap attributes;
        attributes.insert(make_pair("filename", string("test.dll")));
        script.addProperty(new MonoScript(attributes));
    }

    Node root;
    Node script;
};


BOOST_AUTO_TEST_CASE (MonoTestProperty_initialize) {

    MonoTestProperty_testFramework test;

    Node stateNode = test.root.createChild();
    Property::AttributeMap attributes;
    attributes.insert(make_pair("image", string("test")));
    attributes.insert(make_pair("class", string("StaticText")));
    
    BOOST_CHECK_NO_THROW(
        stateNode.addProperty(new MonoTestProperty(attributes))
    );

}


BOOST_AUTO_TEST_CASE (MonoTestProperty_callProperty) {

    MonoTestProperty_testFramework test;

    Node n = test.root.createChild();
    Property::AttributeMap attributes;
    attributes.insert(make_pair("image", string("test")));
    attributes.insert(make_pair("class", string("StaticText")));
    
    n.addProperty(new MonoTestProperty(attributes));

    BOOST_CHECK_EQUAL(n.getProperty<TestInterface>()->getText(), "Constructed");

}


MonoTestProperty::MonoTestProperty(AttributeMap attributes) {

    AttributeMap::const_iterator found;

    found = attributes.find("image");
    if (found == attributes.end()) {
        throw AttributeNotFound("image");
    }
    imageName = boost::any_cast<string>(found->second);

    found = attributes.find("namespace");
    if (found == attributes.end()) {
        namespaceName = string("");
    } else {
        namespaceName = boost::any_cast<string>(found->second);
    }

    found = attributes.find("class");
    if (found == attributes.end()) {
        throw AttributeNotFound("class");
    }
    className = boost::any_cast<string>(found->second);

}


void MonoTestProperty::initialize(const Node& owner) {

    Property::initialize(owner);
 
    // Get a MonoDomain from the ScriptManager to load the assembly into.
    MonoScriptManager* scriptManager =
        owner.getAncestorProperty<MonoScriptManager>();
    // \todo Test for no scriptManager found.

    MonoDomain* domain = scriptManager->getDomain();
    MonoAssembly* assembly = scriptManager->getAssembly(imageName);
    
    // Load the State class from the assembly.
    MonoClass *klass = mono_class_from_name_case(
        mono_assembly_get_image(assembly), 
        namespaceName.c_str(), className.c_str());
    if (!klass) {
        throw runtime_error("Class or namespace not found.");
    }

    object = mono_object_new(domain, klass);

    // This runs the default, argumentless constructor.
    mono_runtime_object_init(object);
}


const string MonoTestProperty::getText() {
   
    MonoClass* klass = mono_object_get_class(object); 
    MonoProperty* prop = mono_class_get_property_from_name (klass, "Text"); 
    MonoString* monoStr = 
        (MonoString*)mono_property_get_value (prop, object, NULL, NULL);
    char* cStr = mono_string_to_utf8(monoStr);
    std::string str(cStr);
    g_free(cStr);
    return str;
}
