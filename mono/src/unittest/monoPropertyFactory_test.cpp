/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <boost/test/auto_unit_test.hpp>
#include <boost/assign/list_of.hpp> // for 'map_list_of()'

#include "MonoPropertyFactory.hpp"
#include "PropertyFactoryTests.hpp"

using namespace std;
using boost::any;
using boost::assign::list_of;
using namespace MidoriGraph;


BOOST_AUTO_TEST_CASE (MonoPropertyFactory_create) {

    create<MonoPropertyFactory>("MonoScriptManager", "MonoScriptManager");
    create<MonoPropertyFactory>("MonoScript", "MonoScript",
        list_of< pair<string, any> > 
            ("filename", string("test.dll"))
        );

    create<MonoPropertyFactory>("MonoScript", "MonoScript",
        list_of< pair<string, any> > 
            ("filename", string("test.dll"))
        );

    create<MonoPropertyFactory>("MonoState", "MonoState",
        list_of< pair<string, any> > 
            ("image", string("test"))
            ("namespace", string(""))
            ("class", string("State"))
        );
}
