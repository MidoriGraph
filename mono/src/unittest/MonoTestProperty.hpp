/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _MONOTESTPROPERTY_HPP_
#define _MONOTESTPROPERTY_HPP_

#include <string>
#include <stack>
#include <boost/bind.hpp>

extern "C" {
    #include <mono/jit/jit.h>
}

#include "Property.hpp"
#include "Node.hpp"
#include "../../../core/src/unittest/TestProperty.hpp"

using namespace boost;
using namespace MidoriGraph;




/// A mock property that changes a string when its functions are called.
class MonoTestProperty : public TestInterface {

  public:

    MonoTestProperty(AttributeMap attibutes);

    static const std::string name() { return "TestProperty"; }       
    virtual const std::string getName() const { return name(); }

    virtual const std::string getText();

    virtual void initialize(const Node& owner);
    
    //virtual void connect(const Node& newParent);
    
    //virtual void disconnect(const Node& oldParent);
    
  private:

    MonoObject* object;

    std::string imageName;
    std::string namespaceName;
    std::string className;
};

#endif //_MONOTESTPROPERTY_HPP_
