/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <boost/test/auto_unit_test.hpp>
#include <boost/assign/list_of.hpp> // for 'map_list_of()'

#include "Node.hpp"
#include "Property.hpp"             // AttributeNotFound
#include "MonoScriptManager.hpp"
#include "MonoScript.hpp"
#include "MonoState.hpp"

using namespace std;
using boost::any;
using boost::assign::map_list_of;
using namespace MidoriGraph;


class MonoState_testFramework {

  public:
    
    MonoState_testFramework() {
        
        root = Node::makeRoot();
        root.addProperty(new MonoScriptManager());

        script = root.createChild();
        Property::AttributeMap attributes;
        attributes.insert(make_pair("filename", string("test.dll")));
        script.addProperty(new MonoScript(attributes));
    }

    Node root;
    Node script;
};


BOOST_AUTO_TEST_CASE (MonoState_initialize) {

    MonoState_testFramework test;

    Node stateNode = test.root.createChild();
    Property::AttributeMap attributes;
    attributes.insert(make_pair("image", string("test")));
    attributes.insert(make_pair("class", string("empty")));
    
    BOOST_CHECK_NO_THROW(
        stateNode.addProperty(new MonoState(attributes))
    );

}


BOOST_AUTO_TEST_CASE (MonoState_initialize_noImage) {

    MonoState_testFramework test;

    Node stateNode = test.root.createChild();
    Property::AttributeMap attributes;
    attributes.insert(make_pair("class", string("empty")));

    BOOST_CHECK_THROW(
        stateNode.addProperty(new MonoState(attributes)),
        AttributeNotFound
    )
}


BOOST_AUTO_TEST_CASE (MonoState_initialize_noClass) {

    MonoState_testFramework test;

    Node stateNode = test.root.createChild();
    Property::AttributeMap attributes;
    attributes.insert(make_pair("image", string("test")));

    BOOST_CHECK_THROW(
        stateNode.addProperty(new MonoState(attributes)),
        AttributeNotFound
    )
}


BOOST_AUTO_TEST_CASE (MonoState_initialize_badImage) {

    MonoState_testFramework test;

    Node stateNode = test.root.createChild();
    Property::AttributeMap attributes;
    attributes.insert(make_pair("image", string("foo")));
    attributes.insert(make_pair("class", string("empty")));

    BOOST_CHECK_THROW(
        stateNode.addProperty(new MonoState(attributes)),
        runtime_error
    )
}


BOOST_AUTO_TEST_CASE (MonoState_initialize_badClass) {

    MonoState_testFramework test;

    Node stateNode = test.root.createChild();
    Property::AttributeMap attributes;
    attributes.insert(make_pair("image", string("test")));
    attributes.insert(make_pair("class", string("bar")));

    BOOST_CHECK_THROW(
        stateNode.addProperty(new MonoState(attributes)),
        runtime_error
    )
}


BOOST_AUTO_TEST_CASE (MonoState_initialize_badNamespace) {

    MonoState_testFramework test;

    Node stateNode = test.root.createChild();
    Property::AttributeMap attributes;
    attributes.insert(make_pair("image", string("test")));
    attributes.insert(make_pair("namespace", string("foobar")));
    attributes.insert(make_pair("class", string("empty")));

    BOOST_CHECK_THROW(
        stateNode.addProperty(new MonoState(attributes)),
        runtime_error
    )
}
