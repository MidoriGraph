/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <boost/test/auto_unit_test.hpp>
#include <boost/assign/list_of.hpp> // for 'map_list_of()'

#include "Node.hpp"
#include "MonoScriptManager.hpp"
#include "MonoScript.hpp"

using namespace std;
using boost::any;
using boost::assign::map_list_of;
using namespace MidoriGraph;


class MonoScript_testFramework {

  public:
    
    MonoScript_testFramework() {
        
        root = Node::makeRoot();
        root.addProperty(new MonoScriptManager());

        child = root.createChild();
        Property::AttributeMap attributes;
        attributes.insert(make_pair("filename", string("test.dll")));
        child.addProperty(new MonoScript(attributes));
        //        map_list_of<string, any>
        //           ("filename", string("test.dll"))
        //    ));
    }

    Node root;
    Node child;
};


BOOST_AUTO_TEST_CASE (MonoScript_initialize) {

    MonoScript_testFramework test;

}

