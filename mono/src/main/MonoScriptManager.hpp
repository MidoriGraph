/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under theterms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _MONO_SCRIPT_MANAGER_HPP_
#define _MONO_SCRIPT_MANAGER_HPP_

extern "C" {
    #include <mono/jit/jit.h>
}
    
#include "Property.hpp"

namespace MidoriGraph {


class MonoScriptManager: public Property {

  public:
    
    MonoScriptManager(AttributeMap attributes = AttributeMap());

    virtual ~MonoScriptManager();

    virtual void initialize(const Node& owner);

    static const std::string name() { return "MonoScriptManager"; }    
    virtual const std::string getName() const { return name(); }

    virtual const InterfaceList getInterfaces() const { 

        InterfaceList interfaces = InterfaceList();
        interfaces.push_back(name());
        return interfaces;
    }  

    MonoDomain* getDomain() {
        return rootDomain.domain;
    }


    MonoAssembly* getAssembly(std::string imageName);

    
  private:

    struct RootDomain {
        RootDomain();
        ~RootDomain();
        MonoDomain* domain;
    };

    static RootDomain rootDomain;

};


} //namespace MidoriGraph

#endif //_MONO_SCRIPT_MANAGER_HPP_

