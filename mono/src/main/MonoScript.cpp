/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

extern "C" {
    #include <mono/jit/jit.h>
    #include <mono/metadata/object.h>
    #include <mono/metadata/assembly.h>
}

#include "MonoScriptManager.hpp"
#include "MonoScript.hpp"

namespace MidoriGraph {

using namespace std;


MonoScript::MonoScript(AttributeMap attributes) {

    filename = getAttribute<string>(attributes, "filename");
}


MonoScript::~MonoScript() {

    
}


void MonoScript::initialize(const Node& owner) {

    Property::initialize(owner);

    /*
    // Load the State class from the assembly.  This is what MonoState needs.
    MonoClass *klass = mono_class_from_name(
        mono_assembly_get_image(assembly), "", "State");
    if (!klass) {
        throw runtime_error(
            (string("State class not found in: ") + filename).c_str());
    }

    // \todo: This part should be in MonoState.
    MonoObject* obj = mono_object_new(domain, klass);
    mono_runtime_object_init(obj);

    */

}


void MonoScript::connect(const Node& newParent) {
 
    // Get a MonoDomain from the ScriptManager to load the assembly into.
    MonoScriptManager* scriptManager =
        owner.getAncestorProperty<MonoScriptManager>();
    // \todo Test for no scriptManager found.

    domain = scriptManager->getDomain();
    if (!domain){
        throw runtime_error("Domain doesn't exist.");
    }

    // Load the assembly into memory.
    assembly = mono_domain_assembly_open (domain, filename.c_str());
    if (!assembly){
        throw runtime_error((string("File not found: ") + filename).c_str());
    }
    

}


void MonoScript::disconnect(const Node& oldParent) {

    mono_assembly_close(assembly);
    assembly = (MonoAssembly*)(0);
}


} //namespace MidoriGraph

