/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

extern "C" {
    #include <mono/jit/jit.h>
    #include <mono/metadata/assembly.h>
    #include <mono/metadata/mono-config.h>
}

#include "MonoScriptManager.hpp"

namespace MidoriGraph {

using namespace std;


MonoScriptManager::MonoScriptManager(AttributeMap attributes) {
    
}


MonoScriptManager::~MonoScriptManager() {


}


void MonoScriptManager::initialize(const Node& owner) {

    Property::initialize(owner);

}


MonoAssembly* MonoScriptManager::getAssembly(string imageName) {

    MonoImage* image = mono_image_loaded(imageName.c_str());

    if (!image) {
        throw runtime_error((string("Image not loaded: ") + imageName).c_str());
    }

    return mono_image_get_assembly(image);
}


MonoScriptManager::RootDomain::RootDomain() {
    mono_assembly_setrootdir(
            "/Library/Frameworks/Mono.framework/Versions/Current/lib");
    mono_set_config_dir (
            "/Library/Frameworks/Mono.framework/Versions/Current/etc");
    //mono_set_dirs(NULL, NULL);
    domain = mono_jit_init("midorigraph");
    mono_config_parse (NULL);
}


MonoScriptManager::RootDomain::~RootDomain() {
    mono_jit_cleanup(domain);
}

MonoScriptManager::RootDomain MonoScriptManager::rootDomain;

} //namespace MidoriGraph

