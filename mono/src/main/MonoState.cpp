/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

extern "C" {
    #include <mono/jit/jit.h>
    #include <mono/metadata/object.h>
    #include <mono/metadata/assembly.h>
}

#include "MonoScriptManager.hpp"
#include "MonoScript.hpp"
#include "MonoState.hpp"

namespace MidoriGraph {

using namespace std;


MonoState::MonoState(AttributeMap attributes) {

    imageName = getAttribute<string>(attributes, "image");
    namespaceName = getAttribute<string>(attributes, "namespace", "");
    className = getAttribute<string>(attributes, "class");
}


MonoState::~MonoState() {

    
}


void MonoState::initialize(const Node& owner) {

    Property::initialize(owner);

    /*

    */

}


void MonoState::connect(const Node& newParent) {
 
    // Get a MonoDomain from the ScriptManager to load the assembly into.
    MonoScriptManager* scriptManager =
        owner.getAncestorProperty<MonoScriptManager>();
    // \todo Test for no scriptManager found.

    MonoDomain* domain = scriptManager->getDomain();
    MonoAssembly* assembly = scriptManager->getAssembly(imageName);
    
    // Load the State class from the assembly.
    MonoClass *klass = mono_class_from_name_case(
        mono_assembly_get_image(assembly), 
        namespaceName.c_str(), className.c_str());
    if (!klass) {
        throw runtime_error("Class or namespace not found.");
    }

    object = mono_object_new(domain, klass);

    // This runs the default, argumentless constructor.
    mono_runtime_object_init(object);
}


void MonoState::disconnect(const Node& oldParent) {

}


} //namespace MidoriGraph

