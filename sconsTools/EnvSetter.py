import copy


class EnvSetter:
    def __init__(self, env):
        self.env = env
        self.headers = []
        self.libraries = []
        self.platformHeaderPaths = []
        self.platformLibPaths = []

    def Clone(self):
        return copy.copy(self)

    def getEnv(self):
        env = self.env.Clone()
        
        for path in self.platformHeaderPaths:
            if (env.get("PLATFORM", None) == path[0]):
                env.Append(CPPPATH=path[1])
        for path in self.platformLibPaths:
            if (env.get("PLATFORM", None) == path[0]):
                env.Append(LIBPATH=path[1])

        conf = env.Configure()
        for header in self.headers:
            if not conf.CheckCXXHeader(header):
                print header + " not found!"
                return None
        for library in self.libraries:
            if not conf.CheckLib(library):
                print library + " not found!"
        return conf.Finish()
