import os
import SCons.Builder
import SCons.Node.FS
import SCons.Util


#***** Test results builder
def builder_unit_test(target, source, env):
    app = str(source[0].abspath)
    olddir = os.getcwd()
    os.chdir(str(source[0].get_dir()))
    if os.spawnl(os.P_WAIT, app, app)==0:
        os.chdir(olddir)
        open(str(target[0]),'w').write("PASSED\n")
    else:
        os.chdir(olddir)
        return 1

TestBuilder = SCons.Builder.Builder(action = builder_unit_test)

def generate(env):
    env['BUILDERS']['Test'] = TestBuilder

def exists(env):
    return true
