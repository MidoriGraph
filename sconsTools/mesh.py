import os.path
import SCons.Builder
import SCons.Node.FS
import SCons.Util

meshcom = "$OGREXMLCONVERTER $SOURCES ${TARGET.abspath}"

MeshBuilder = SCons.Builder.Builder(action = '$MESHCOM',
    source_factory = SCons.Node.FS.default_fs.Entry, suffix = '')

def generate(env):
    env['BUILDERS']['OgreMesh'] = MeshBuilder

    env['OGREXMLCONVERTER']        = 'OgreXMLConverter'
    env['MESHCOM']     = SCons.Action.Action(meshcom)

def exists(env):
    return internal_zip or env.Detect('OgreXMLConverter')
