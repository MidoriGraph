/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _TASKMANAGER_H_
#define _TASKMANAGER_H_

#include <string>
#include <boost/function.hpp>
#include <boost/any.hpp>

#include "Property.hpp"
#include "utility.hpp"      //Scalar

namespace MidoriGraph {


/// Interface for properties that control the frame by frame execution of tasks.
class TaskManager : public Property {

  public:
    
    typedef boost::function<void (Scalar deltaTime)> TaskFunction;
    
    /// Used by other objects to reference Tasks in the manager.
    /// Because each implimentation will have different ways of tracking Tasks,
    /// boost::any is used to allow any type of data to be a handle.
    typedef boost::any TaskHandle;
    
    /// \note This interface class cannot be instantiated and so does not 
    /// impliment getName().
    static const std::string name() { return "TaskManager"; }    
    
    virtual const InterfaceList getInterfaces() const { 

        InterfaceList interfaces = InterfaceList();
        interfaces.push_back(name());
        return interfaces;
    }  

    
    /// Adds a task to be managed by the manager.
    /// \return a TaskHandle referencing the added task.
    /// \todo Should throw on failure?
    virtual TaskHandle addTask(TaskFunction task) = 0;
    
    /// Remove a task from the manager.
    virtual void removeTask(TaskHandle task) = 0;
    
    /// Start executing tasks.
    virtual void start() = 0;
    
    /// Stop executing tasks.
    virtual void stop() = 0;

};


} //namespace MidoriGraph

#endif //_TASKMANAGER_H_
