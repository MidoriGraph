/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _UTILITY_HPP_
#define _UTILITY_HPP_

#include <stdexcept>                // runtime_error

namespace MidoriGraph {


class PropertyNotFound : public std::runtime_error {

  public:
  
    PropertyNotFound(std::string propertyName) : 
        runtime_error("Property not found: " + propertyName) {}

};


typedef float Scalar;

struct Vector3 {
    Vector3(Scalar x, Scalar y, Scalar z) : x(x), y(y), z(z) {}
    Scalar x;
    Scalar y;
    Scalar z;
};

struct Vector4 {
    Vector4(Scalar w, Scalar x, Scalar y, Scalar z) : w(w), x(x), y(y), z(x) {}
    Scalar w;    
    Scalar x;
    Scalar y;
    Scalar z;
};
    
template<class T>
void del(const T& t) {
    delete t;
}


template<class T>
void del_second(const T& t) {
    delete t.second;
}


} //namespace MidoriGraph
#endif //_UTILITY_HPP_
