/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#include <utility>

#include "LinearTaskManager.hpp"

namespace MidoriGraph {

using namespace std;

LinearTaskManager::LinearTaskManager(AttributeMap attributes) : taskNumber(0) {
}


LinearTaskManager::~LinearTaskManager() {
}


void LinearTaskManager::initialize(const Node& owner) {

    Property::initialize(owner);

}


TaskManager::TaskHandle LinearTaskManager::addTask(TaskFunction task) {
    
    ++taskNumber;
    tasks.insert(make_pair(taskNumber, task));
    return TaskHandle(taskNumber);
}


void LinearTaskManager::removeTask(TaskHandle task) {
    
    // Search for the handle in the task map.
    TaskMap::iterator found =
        tasks.find(boost::any_cast<TaskHandleType>(task));
    
    // Ensure the task was found.  Removing a nonexistant task is bad.
    if(found == tasks.end()) {
        throw std::runtime_error("Bad task handle or task already removed!");
    }
    
    tasks.erase(found);
}


void executeTask(LinearTaskManager::TaskMap::value_type taskPair) {
    taskPair.second(1.0);
}


void LinearTaskManager::start() {
    continueExecuting = true;
    while (continueExecuting) {
        for_each(tasks.begin(), tasks.end(), &executeTask);
    }
}


void LinearTaskManager::stop() {
    continueExecuting = false;
}


} //namespace MidoriGraph
