/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under theterms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include "Property.hpp"
#include "utility.hpp"

namespace MidoriGraph {


class Transform : public Property {
    
  public:
    Transform(Vector3 translate):
        translate(translate), rotate(0,0,0,1) {}
    
    Transform(Vector3 translate, Vector4 rotate):
        translate(translate), rotate(rotate) {}
    
    Transform(const Property::AttributeMap& attributes =
              Property::AttributeMap())
        : translate(0,0,0), rotate(0,0,0,1) {
        
        translate = getAttribute<Vector3>(attributes, "translate",
                                         Vector3(0,0,0));
        rotate = getAttribute<Vector4>(attributes, "rotate",
                                      Vector4(0,0,0,1));
        }
    
    virtual ~Transform() {}

    static std::string name() { return "Transform"; }       
    virtual const std::string getName() const { return name(); }
    virtual const InterfaceList getInterfaces() const { 
        InterfaceList interfaces = InterfaceList();
        interfaces.push_back(name());
        return interfaces;
    }  
    
    virtual void initialize(const Node& owner) {
        Property::initialize(owner);
    }
    
    Vector3 getTranslate() { return translate; }
    Vector4 getRotate() { return rotate; }
    
  private:

    Vector3 translate;
    Vector4 rotate;
    
};


} //namespace MidoriGraph

#endif //_TRANSFORM_H_
