/*
 *  Node.cpp
 *  CoreBGL
 *
 *  Created by James Marble on 6/18/07.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */
#include <cassert>
#include <algorithm>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>

#include "Node.hpp"
#include "Property.hpp"

namespace MidoriGraph {

using namespace std;    
using namespace boost;
using namespace boost::lambda;



Node::Node(const shared_ptr<Entity>& entity) : entity(entity) {
}


Node Node::makeRoot() {
    shared_ptr<Entity> entity(new Entity);
    entity->g = shared_ptr<Graph>(new Graph);
    entity->v = add_vertex(*(entity->g));
    // Set vertex data to a shared_ptr to the new Entity.
    (*entity->g)[entity->v] = entity;
    return Node(entity);
}


Node Node::createChild() const {
    assert(entity);
    assert(entity->g);
    //assert(entity->v != Entity::Graph::null_vertex_descriptor());
    
    shared_ptr<Entity> child(new Entity);
    child->v = add_vertex(*(entity->g));
    child->g = entity->g;
    (*child->g)[child->v] = child;
    add_edge(entity->v, child->v, *entity->g);
    return Node(child);
}



void Node::addProperty(shared_ptr<Property> p) {

    Property::InterfaceList interfaces = p->getInterfaces();
    for(Property::InterfaceList::iterator interface = interfaces.begin();
        interface != interfaces.end(); ++interface) {
        entity->properties.insert(make_pair(*interface, p));
    } 

    p->initialize(*this);

    // Allow the property to link to other properties by notifying it that it
    // is in a graph.
    Node parent = getParent();
    if (parent.valid()) { 
        p->connect(parent);
    }
}


void Node::addProperty(Property* p) {
    addProperty(shared_ptr<Property>(p));
}


shared_ptr<Property> Node::getProperty(const std::string name) const {

    Properties::iterator p = entity->properties.find(name);
    if (p == entity->properties.end()) {
        return shared_ptr<Property>();
    }
    
    return p->second;
}


Node Node::getParent() const {
    assert(entity);
    
    // If not in a graph it can't have a parent.
    if (!entity->g) {
        return Node();
    }
    
    std::pair<Graph::in_edge_iterator, Graph::in_edge_iterator> inEdges =
        in_edges(entity->v, *entity->g);
    
    // Check for lack of parent (root).
    if (inEdges.first == inEdges.second) {
        // Invalid (empty) node indicates no parent to get.
        return Node();
    }
    
    // Just pick the first "parent" if more than one.
    //return Node(source(*(inEdges.first), g));
    Graph::in_edge_iterator parentEdgeIter = inEdges.first;
    Graph::edge_descriptor parentEdge = *parentEdgeIter;
    Graph::vertex_descriptor parentVertex = source(parentEdge, *entity->g);
    return Node((*entity->g)[parentVertex]);
    
}


/// \todo replace with "ancestorIterator"
shared_ptr<Property> Node::getAncestorProperty(const std::string name) const {

    shared_ptr<Property> property;
    Node node(this->getParent());
    
    while (node.valid()) {
        
        property = node.getProperty(name);
        if (property) {
            return property;
        }
        node = node.getParent();
    }
        
    // Property not found.
    return shared_ptr<Property>();
}


template <class G>
void removeTree(typename graph_traits<G>::vertex_descriptor v, G& g) {
    
    std::pair<typename graph_traits<G>::out_edge_iterator,
    typename graph_traits<G>::out_edge_iterator> outEdges = out_edges(v, g);
    
    // destroy all descedant verticies recursively first.
    typename graph_traits<G>::out_edge_iterator vi, vi_end, next;
    tie(vi, vi_end) = outEdges;
    for (next = vi; vi != vi_end; vi = next) {
        ++next;
        removeTree(target(*vi, g), g);
    }
    
    // remove this vertex
    remove_vertex(v, g);
    
}


void Node::remove() {
    
    // Remove the edge from the parent
    Graph::in_edge_iterator inEdges, inEdgesEnd;
    tie(inEdges, inEdgesEnd) = in_edges(entity->v, *entity->g);
    if (inEdges != inEdgesEnd) {
        //remove_edge(source(*inEdges, g), target(*inEdges, g), g);
        remove_edge(*inEdges, *entity->g);
    }
    
    // Remove this vertex and all descendants.
    // destroy all descedant verticies recursively first.    
    std::pair<graph_traits<Graph>::out_edge_iterator,
        graph_traits<Graph>::out_edge_iterator> outEdges = out_edges(
        entity->v, *entity->g);
    graph_traits<Graph>::out_edge_iterator vi, vi_end, next;
    tie(vi, vi_end) = outEdges;
    for (next = vi; vi != vi_end; vi = next) {
        ++next;
        Node((*entity->g)[target(*vi, *entity->g)]).remove();
    }
    
    // remove this vertex
    remove_vertex(entity->v, *entity->g);
    

    entity->v = Vertex();
    entity->g = shared_ptr<Graph>();
    
    
    //remove all properties
    while (!entity->properties.empty()) {
         removeProperty(entity->properties.begin()->second);
    }
    
    entity = shared_ptr<Entity>(); 
}


void Node::move(const Node destination) const {

    assert(destination.valid());
    
    Node currentParent = getParent();
    assert(getParent().valid());

    // Disconnect all properties.
    for (Node::Properties::iterator pos = entity->properties.begin();
         pos != entity->properties.end(); ++pos) {
        pos->second->disconnect(currentParent);
    }
    
    // Change link to new parent.
    remove_edge(currentParent.entity->v, entity->v, *entity->g);
    add_edge(destination.entity->v, entity->v, *entity->g);

    // Connect all properties.
    for (Node::Properties::iterator pos = entity->properties.begin();
         pos != entity->properties.end(); ++pos) {
        pos->second->connect(destination);
    }
}


void Node::removeProperty(const shared_ptr<Property> property) const {

    if (!numProperties()){
        // throw?
        return;    
    }
    
    bool found = false;
    
    // Safely remove all entries in the property map with the given shared_ptr.
    // This is the method described in The C++ Standard Library, 6.6.2.
    for (Node::Properties::iterator pos = entity->properties.begin();
         pos != entity->properties.end(); ) {

        if (pos->second == property) {
            entity->properties.erase(pos++);
            found = true;
        } else {
            ++pos;
        }
    }

    if (found) {
        property->disconnect(*this);
    } else {
        //throw?
    }
    
    
}


} //namespace MidoriGraph
