/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under theterms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _LINEARTASKMANAGER_H_
#define _LINEARTASKMANAGER_H_

#include <string>
#include <map>
#include <boost/function.hpp>
#include <boost/any.hpp>

#include "utility.hpp"      //Scalar
#include "TaskManager.hpp"

namespace MidoriGraph {


class LinearTaskManager : public TaskManager {

  public:
    
    LinearTaskManager(AttributeMap attributes = AttributeMap());

    virtual ~LinearTaskManager();

    virtual void initialize(const Node& owner);

    static const std::string name() { return "LinearTaskManager"; }    
    virtual const std::string getName() const { return name(); }

    // This property does not expand on the TaskManager interface and so does
    // not add another interface to the interface list.
    
    virtual TaskHandle addTask(TaskFunction task);

    virtual void removeTask(TaskHandle task);
    
    virtual void start();
    
    virtual void stop();

  private:

    typedef int TaskHandleType;
    
    bool continueExecuting;

    TaskHandleType taskNumber;
    
    typedef std::map<TaskHandleType, TaskFunction> TaskMap;
    TaskMap tasks;
    
};

} //namespace MidoriGraph

#endif //_LINEARTASKMANAGER_H_
