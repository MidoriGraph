/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.
 
MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.
 
MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _NODE_HPP_
#define _NODE_HPP_

#include <string>
#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/graph/adjacency_list.hpp>

#include "utility.hpp" //PropertyNotFound() exception.

namespace MidoriGraph {

// Declared here to prevent circular dependency.
class Property;
    
/// A "handle" to a property containing entity.
/// Contains a shared_ptr to an entity which contains all the properties of a
/// simulated object as well as it's location in the graph.
class Node {

  public:

    /// Default constructor creates a null node.
    Node(): entity(boost::shared_ptr<Entity>()) {}
    
    /// Creates a new graph and its root entity.
    /// \return the new root node.
    static Node makeRoot();
    
    /// Creates a new entity as a child of this one.
    Node createChild() const;
    /// Destructor does nothing.
    ~Node() {}
    
    /// Checks if node is valid.
    /// \return true if a valid entity exists.
    bool valid() const {
        return entity;  // cast to bool
    }
    
    /// Checks if two nodes are the same entity.
    bool operator==(const Node& r) const {
        return this->entity == r.entity;
    }
    
    /// \return this node's parent in the graph, invalid Node if there is none.
    Node getParent() const;
    
    /// \return the number of children this node has.
    int inline numChildren() const {
        return boost::out_degree(entity->v, *entity->g);
    }
    
    /// Change the parent of this node.
    void move(const Node destination) const;
    
    /// Remove the node and all children from the graph.
    void remove();
    
    /// @{
    /// Property interface
    
    /// Adds a property to the entity.
    /// \todo This should throw (in DEBUG at least) if identical keys are added.
    void addProperty(boost::shared_ptr<Property> p);
    void addProperty(Property* p);
    
    /// Searches the property map for a property with the given name.
    /// \param name Name of the property to search for.
    /// \return a shared_ptr to the property if found.
    /// If no property with that name is found an empty shared_ptr is returned.
    boost::shared_ptr<Property> getProperty(std::string name) const;
    
    /// Searches the property map for a property of the given type.
    /// The template parameter T is the property type to search for.
    /// \return a pointer to the derived property if found.
    /// \throw PropertyNotFound If no property of the given type is found.
    /// This allows getProperty<>() to be used in constructs like:
    /// \c node.getProperty<SomeProperty>.doSomething()
    /// If a null pointer was returned instead checking would have to be done.
    template <class T>
    T* getProperty() const {
        boost::shared_ptr<Property> property = getProperty(T::name());
        if (!property) {
            throw PropertyNotFound(T::name());
        }
        T* t = boost::dynamic_pointer_cast<T>(property).get();
        if (!t) {
            throw PropertyNotFound(T::name());
        }
        return t;
    }
   
    /// Searches this node and all ancestors for a property with the given name.
    /// \param name Name of the property to search for.
    /// \return a shared_ptr to the property if found.
    /// If no property with that name is found an empty shared_ptr is returned.
    boost::shared_ptr<Property> getAncestorProperty(std::string name) const;
    
    /// Searches the property map for a property of the given type.
    /// The template parameter T is the property type to search for.
    /// \return a pointer to the derived property if found.
    /// \throw PropertyNotFound If no property of the given type is found.
    template <class T>
    T* getAncestorProperty() const {
        boost::shared_ptr<Property> property = 
            getAncestorProperty(T::name());
        if (!property) {
            throw PropertyNotFound(T::name());
        }
        T* t = boost::dynamic_pointer_cast<T>(property).get();
        if (!t) {
            throw PropertyNotFound(T::name());
        }
        return t;
    }
    
    /// Removes a property with the given name.
    /// \param property The property to search for and remove.
    /// \note Will silently fail if no property of the given name is found.
    void removeProperty(const boost::shared_ptr<Property> property) const;
    
    /// \return The number of properties in the property map.
    int inline numProperties() const {
        return entity->properties.size();
    }
    
    /// @}
 
  private:
    
    /// \note: vecS can not be used for the vectors because it would be
    /// inefficient to remove them from the graph.  Also, removing a node 
    /// would invalidate all vertex_descriptors.
    typedef boost::adjacency_list<boost::listS, boost::listS, 
                                  boost::bidirectionalS, Node> Graph;
    
    typedef Graph::vertex_descriptor Vertex;
    typedef Graph::vertex_iterator VertexIter;

    typedef std::map<std::string, boost::shared_ptr<Property> > Properties;
        
    struct Entity {
        
        /// \todo Consider replacing associative containers with sorted vectors.
        /// See Item 23 of Effective STL.  Also has a space saving benifit:
        /// property names can be static class members or even methods.
        Node::Properties properties;
        
        boost::shared_ptr<Node::Graph> g;

        Node::Vertex v;
        
    };

        
    Node(const boost::shared_ptr<Entity>& entity);    
    
    boost::shared_ptr<Entity> entity;
    
};


} //namespace MidoriGraph

#endif //_NODE_HPP_
