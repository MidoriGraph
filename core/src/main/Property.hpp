/* 
Copyright 2005, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _PROPERTY_HPP_
#define _PROPERTY_HPP_

#include <stdexcept>                // runtime_error
#include <typeinfo>                 // bad_cast
#include <list>                     // PropertyNameList
#include <map>                      // AttributeMap
#include <string>
#include <boost/utility.hpp>        // noncopyable
#include <boost/any.hpp>

#include "Node.hpp"

namespace MidoriGraph {

/// Base class for an object that defines an aspect of a node.
/// Properties actually define what a particular node is, how it
/// behaves, and what it looks like.
///
/// Derived classes must impliment all of the pure virtual functions defined
/// here, as well as a static function called name taking no arguments and
/// returning a string.  This function must be defined or various function
/// templates that require it will fail to compile.
/// \ingroup core
class Property : public boost::noncopyable {

  public:
  
    /// Concrete derivations will use AttributeMaps to initialize.
    typedef std::map<std::string, boost::any> AttributeMap;

    /// Virtual destructor required because there are virtual methods.
    virtual ~Property() {}

    /// Called by Node when added to the properties map.
    /// \param owner The Node this property will belong to.
    /// \return name of Property subclass
    /// \todo throw if bad owner or already initialized
    virtual void initialize(const Node& owner) {
        this->owner = owner;
    }
   
    /// Called by Node when it is added as a child.
    virtual void connect(const Node& newParent) {}
    
    /// Called by Node when it is removed from its parent.
    virtual void disconnect(const Node& oldParent) {}
    
    /// Returns the name of the properties class.
    /// Useful for debugging, scripting, and applications which require intro-
    /// spection (graph viewer, etc...).
    virtual const std::string getName() const = 0;
    
    typedef std::list<std::string> InterfaceList;
    /// Returns all interfaces that a property implements.
    virtual const InterfaceList getInterfaces() const = 0; 

  protected:

    /// The Entity this Property is associated with.
    Node owner;

};


class AttributeNotFound : public std::runtime_error {

  public:
  
    AttributeNotFound(std::string attributeName) : 
        runtime_error("Attribute not found: " + attributeName) {}

};


class BadAttributeCast : public std::runtime_error {

  public:
  
    BadAttributeCast(std::string attributeName) : 
        runtime_error("Bad attribute cast of " + attributeName) {}

};


template<class T>
T getAttribute(const Property::AttributeMap& attributes, std::string name) {

    Property::AttributeMap::const_iterator attrIter(attributes.find(name));
    if(attrIter == attributes.end()) {
        throw AttributeNotFound(name);
    }
    
    try {
        return boost::any_cast<T>(attrIter->second);
    } catch (boost::bad_any_cast e) {
        throw(BadAttributeCast(name));
    }

}


template<class T>
T getAttribute(const Property::AttributeMap& attributes, std::string name,
               const T& defaultValue) {

    Property::AttributeMap::const_iterator attrIter(attributes.find(name));
    if(attrIter == attributes.end()) {
        return defaultValue;
    }
    
    try {
        return boost::any_cast<T>(attrIter->second);
    } catch (boost::bad_any_cast e) {
        throw(BadAttributeCast(name));
    }

}

} //namespace MidoriGraph
#endif				//_PROPERTY_HPP_
