/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
 This file is part of MidoriGraph.
 
 MidoriGraph is free software; you can redistribute it and/or modify it under the 
 terms of the GNU General Public License as published by the Free Software 
 Foundation; either version 2 of the License, or (at your option) any later
 version.
 
 MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License along with
 MidoriGraph; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
 
#ifndef _PROPERTY_FACTORY_H_
#define _PROPERTY_FACTORY_H_


#include "Property.hpp"
 
namespace MidoriGraph {




class PropertyFactory {
 
  public:
  
  
    /// Creates a Property suitable for adding to a Node.
    /// \param propertyType Type of property to create.
    /// \param attributes Arguments for creation of property.
    /// \return A new property b.
    virtual Property* create(
        std::string propertyType,
        Property::AttributeMap attributes = Property::AttributeMap()) {

        PropertyTypeMapIter typeIter = propertyTypes.find(propertyType);
        if(typeIter == propertyTypes.end()) {
            throw PropertyNotFound(propertyType);
        }
        
        // Call the function pointer to create the Property.
        return (*(*typeIter->second))(attributes);
        
    }
 
  protected:
  
    /// Creates a Property of subclass T.
    /// Function pointers to instances of this template are stored in the
    /// PropertyTypeMap then called when requested.
    /// \param attributes Contains any arguments to the initializer.
    /// \return A pointer to the Property base class.
    template<class T>
    static Property* createProperty(const Property::AttributeMap& attributes) {
        
        return new T(attributes);
    }

    /// Function pointer to a creator function.
    typedef Property*(*CreatePropertyFuncPtr)(const Property::AttributeMap&);
    
    typedef std::map<std::string, CreatePropertyFuncPtr> PropertyTypeMap;
    
    /// 
    template<class T>
    void addPropertyType(const std::string name = T::name()) {
        propertyTypes.insert(make_pair(name, &createProperty<T>));
    }
    
    
    typedef PropertyTypeMap::iterator PropertyTypeMapIter;
    
    PropertyTypeMap propertyTypes;
    
};


} //namespace MidoriGraph
#endif //_PROPERTY_FACTORY_H_
