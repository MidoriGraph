/* 
Copyright 2005, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <boost/test/auto_unit_test.hpp>

#include "Node.hpp"

using namespace MidoriGraph;

using boost::unit_test::test_suite;


BOOST_AUTO_TEST_CASE (Node_defaultConstructor_valid) {
    
    Node invalid;
    
    BOOST_CHECK(!invalid.valid());
}


BOOST_AUTO_TEST_CASE (Node_makeRoot) {

    Node root(Node::makeRoot());

    BOOST_CHECK(root.valid());
}


BOOST_AUTO_TEST_CASE (Node_copyConstructor) {

    Node root(Node::makeRoot());
    Node copy(root);

    BOOST_CHECK(copy.valid());
    BOOST_CHECK(copy == root);
}


BOOST_AUTO_TEST_CASE (Node_assignmentOperator) {

    Node root(Node::makeRoot());
    Node copy;
    copy = root;

    BOOST_CHECK(copy.valid());
    BOOST_CHECK(copy == root);
}


BOOST_AUTO_TEST_CASE (Node_remove_single) {
    
    Node root(Node::makeRoot());
    
    root.remove();
    BOOST_CHECK(!root.valid());
    
}


BOOST_AUTO_TEST_CASE (Node_copyConstructor_equals) {
    
    Node root(Node::makeRoot());
    Node copy(root);
    
    BOOST_REQUIRE(copy.valid());
    BOOST_CHECK(root == copy);
}


BOOST_AUTO_TEST_CASE (Node_createChild_getParent) {
    
    Node root(Node::makeRoot());
    
    Node child(root.createChild());
    BOOST_REQUIRE(child.valid());
    
    Node parent(child.getParent());
    BOOST_REQUIRE(parent.valid());
    BOOST_CHECK(root == parent);
    
    BOOST_CHECK(!(root.getParent()).valid());
}


BOOST_AUTO_TEST_CASE (Node_numChildren) {
    
    Node root(Node::makeRoot());
    
    root.createChild();
    BOOST_CHECK_EQUAL(root.numChildren(),1);
    
    Node child(root.createChild());
    child.createChild();
    child.createChild();
    child.createChild();
    BOOST_CHECK_EQUAL(child.numChildren(),3);
    BOOST_CHECK_EQUAL(root.numChildren(),2);
    
}


BOOST_AUTO_TEST_CASE (Node_remove_multiple) {
    
    Node root(Node::makeRoot());
    //Ensure that there are no problems iterating deeply.
    Node grandchild = root.createChild().createChild();
    //Ensure there are no problems iterating across children.
    grandchild.createChild();
    grandchild.createChild();

    root.remove();    
    BOOST_CHECK(!root.valid());

    // Ensure siblings aren't effected.
    root = Node::makeRoot();
    Node child1 = root.createChild();
    Node child2 = root.createChild();
    
    child1.remove();    
    BOOST_CHECK(!child1.valid());
    BOOST_CHECK(child2.valid());
    BOOST_CHECK_EQUAL(root.numChildren(), 1);
}  


BOOST_AUTO_TEST_CASE (Node_move) {

    Node root(Node::makeRoot());
    Node node1(root.createChild());
    Node node2(root.createChild());
    node1.move(node2);

    BOOST_CHECK(node1.getParent() == node2);
    BOOST_CHECK_EQUAL(node2.numChildren(), 1);
    BOOST_CHECK_EQUAL(root.numChildren(), 1);
}


BOOST_AUTO_TEST_CASE (Node_sizeof) {

    // Node should be light as it acts like a pointer.
    // It would be preferable to be size 1, but we need a pointer to the graph.
    BOOST_WARN_EQUAL(sizeof(Node), 2);
}

