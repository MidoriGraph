/* 
Copyright 2005, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include <boost/test/auto_unit_test.hpp>

#include "Property.hpp"
#include "Node.hpp"

#include "TestProperty.hpp"

using boost::unit_test::test_suite;

using namespace MidoriGraph;
using namespace boost;


BOOST_AUTO_TEST_CASE (Property_new) {

    shared_ptr<Property> property(new TestProperty("foo"));

    BOOST_REQUIRE(property);
    BOOST_CHECK_EQUAL(dynamic_pointer_cast<TestInterface>(property)->getText(),
                      "foo");
}


BOOST_AUTO_TEST_CASE (Node_addProperty) {
    
    Node root(Node::makeRoot());
    
    root.addProperty(shared_ptr<Property>(new TestProperty()));
    
    BOOST_CHECK_EQUAL(root.numProperties(), 1);
}


BOOST_AUTO_TEST_CASE (Node_addProperty_redundant) {
    
    Node root(Node::makeRoot());
    
    root.addProperty(shared_ptr<Property>(new TestProperty("foobaz")));
    
    // Should silently ignore addition of another property of the same name?
    root.addProperty(shared_ptr<Property>(new TestProperty("barbaz")));
    
    BOOST_CHECK_EQUAL(root.numProperties(), 1);
    
    TestInterface* property = root.getProperty<TestInterface>();
    BOOST_REQUIRE(property);
    BOOST_CHECK_EQUAL(property->getText(), "foobaz");
}


BOOST_AUTO_TEST_CASE (Node_getProperty) {
    
    Node root(Node::makeRoot());

    root.addProperty(new TestProperty("bar"));

    shared_ptr<Property> property(root.getProperty("TestInterface"));
    BOOST_REQUIRE(property);
    BOOST_CHECK_EQUAL(dynamic_pointer_cast<TestInterface>(property)->getText(),
                      "bar");
}


BOOST_AUTO_TEST_CASE (Node_getProperty_fail) {
    
    Node root(Node::makeRoot());

    shared_ptr<Property> property = root.getProperty("TestInterface");
    BOOST_CHECK(!property);
}


BOOST_AUTO_TEST_CASE (Node_getProperty_template) {
    
    Node root(Node::makeRoot());

    root.addProperty(new TestProperty("bar"));

    TestInterface* testProperty;
    BOOST_REQUIRE_NO_THROW(testProperty = root.getProperty<TestInterface>());
    BOOST_CHECK_EQUAL(testProperty->getText(), "bar");
}

BOOST_AUTO_TEST_CASE (Node_getProperty_template_fail) {
    
    Node root(Node::makeRoot());

    BOOST_CHECK_THROW(root.getProperty<NoExistTestProperty>(),
                      PropertyNotFound);
}


BOOST_AUTO_TEST_CASE (Node_removeProperty) {

    Node root(Node::makeRoot());
    root.addProperty(new TestProperty());

    root.removeProperty(root.getProperty("TestProperty"));

    BOOST_CHECK(!root.getProperty("TestProperty"));
    
}


BOOST_AUTO_TEST_CASE (Node_getAncestorProperty) {
    
    Node root(Node::makeRoot());
    Node child(root.createChild());

    // Prefered method throws on failure.
    BOOST_CHECK_THROW(child.getAncestorProperty<TestInterface>(),
                      PropertyNotFound);
    BOOST_CHECK_THROW(child.getAncestorProperty<NoExistTestProperty>(),
                      PropertyNotFound);
    
    shared_ptr<Property> property = child.getAncestorProperty("TestInterface");
    BOOST_CHECK(!property);

    // addProperty
    root.addProperty(new TestProperty("bar"));

    // Prefered method returns correct pointer type.
    TestInterface* testProperty;
    testProperty = child.getAncestorProperty<TestInterface>();
    BOOST_CHECK_EQUAL(testProperty->getText(), "bar");
    
    // Nonprefered method returns shared_ptr<Property>.
    property = child.getAncestorProperty("TestInterface");
    BOOST_REQUIRE(property);
    BOOST_CHECK_EQUAL(dynamic_pointer_cast<TestInterface>(property)->getText(),
                      "bar");
    
    // Prefered method
    BOOST_CHECK_THROW(child.getAncestorProperty<NoExistTestProperty>(),
                  PropertyNotFound);
    
    // Nonprefered method
    property = child.getAncestorProperty("NoExistTestProperty");
    BOOST_CHECK(!property);
}


BOOST_AUTO_TEST_CASE (Property_initialize) {

    Node root(Node::makeRoot());
    root.addProperty(new StatusReporterTestProperty());

    TestInterface* p = root.getProperty<TestInterface>();
    BOOST_CHECK_EQUAL(p->getText(), "initialized");
    BOOST_CHECK_EQUAL(p->getText(), "constructed");
}


BOOST_AUTO_TEST_CASE (Property_connect) {

    Node root(Node::makeRoot());
    Node child(root.createChild());
    child.addProperty(new StatusReporterTestProperty());

    TestInterface* p = child.getProperty<TestInterface>();
    BOOST_CHECK_EQUAL(p->getText(), "connected");
    BOOST_CHECK_EQUAL(p->getText(), "initialized");
    BOOST_CHECK_EQUAL(p->getText(), "constructed");
}


BOOST_AUTO_TEST_CASE (Property_disconnect) {

    Node root(Node::makeRoot());
    Node child(root.createChild());
    shared_ptr<Property> property(new StatusReporterTestProperty());
    child.addProperty(property);
    child.removeProperty(property);

    TestInterface* p = &*dynamic_pointer_cast<TestInterface>(property);
    BOOST_CHECK_EQUAL(p->getText(), "disconnected");
    BOOST_CHECK_EQUAL(p->getText(), "connected");
    BOOST_CHECK_EQUAL(p->getText(), "initialized");
    BOOST_CHECK_EQUAL(p->getText(), "constructed");
}


BOOST_AUTO_TEST_CASE (Property_remove_owner) {

    Node root(Node::makeRoot());
    Node child(root.createChild());
    shared_ptr<Property> property(new StatusReporterTestProperty());
    child.addProperty(property);
    child.remove();

    TestInterface* p = &*dynamic_pointer_cast<TestInterface>(property);
    BOOST_CHECK_EQUAL(p->getText(), "disconnected");
    BOOST_CHECK_EQUAL(p->getText(), "connected");
    BOOST_CHECK_EQUAL(p->getText(), "initialized");
    BOOST_CHECK_EQUAL(p->getText(), "constructed");
}


BOOST_AUTO_TEST_CASE (Property_remove_owner_tree) {

    Node root(Node::makeRoot());
    Node child(root.createChild());
    Node grandchild(child.createChild());
    shared_ptr<Property> property(new StatusReporterTestProperty());
    grandchild.addProperty(property);
    child.remove();

    TestInterface* p = &*dynamic_pointer_cast<TestInterface>(property);
    BOOST_CHECK_EQUAL(p->getText(), "disconnected");
    BOOST_CHECK_EQUAL(p->getText(), "connected");
    BOOST_CHECK_EQUAL(p->getText(), "initialized");
    BOOST_CHECK_EQUAL(p->getText(), "constructed");
}


BOOST_AUTO_TEST_CASE (Property_move_owner) {

    Node root(Node::makeRoot());
    Node node1(root.createChild());
    Node node2(root.createChild());
    node1.addProperty(new StatusReporterTestProperty());
    node1.move(node2);

    TestInterface* p = node1.getProperty<TestInterface>();
    BOOST_CHECK_EQUAL(p->getText(), "connected");
    BOOST_CHECK_EQUAL(p->getText(), "disconnected");
    BOOST_CHECK_EQUAL(p->getText(), "connected");
    BOOST_CHECK_EQUAL(p->getText(), "initialized");
    BOOST_CHECK_EQUAL(p->getText(), "constructed");
}

