/* 
Copyright 2005, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _TESTPROPERTY_HPP_
#define _TESTPROPERTY_HPP_

#include <string>
#include <stack>
#include <boost/bind.hpp>

#include "Property.hpp"
#include "Node.hpp"

using namespace boost;
using namespace MidoriGraph;


/// A mock interface to a property that holds a string for testing.
class TestInterface : public Property {
   
  public:
    
    /// \note Virtual interfaces (that is, classes that will not be instantiated
    /// ) are still required to impliment name() to allow function templates
    /// that need a name in string form to compile.
    static const std::string name() { return "TestInterface"; }
      
    /// \note Properties that impliment this interface could simply create a
    /// list locally, but that would create a maintainance problem if this
    /// interface was changed somehow to include another interface.  For example
    /// if a Car interface was refactored into a Vehicle interface and a Car
    /// interface that derived from the Vehicle interface.
    virtual const InterfaceList getInterfaces() const { 
        InterfaceList interfaces = InterfaceList();
        interfaces.push_front("TestInterface");
        return interfaces;
    }
  
    /// Return a test string.
    /// This is the function that every property that impliments this interface
    /// must impliment. 
    virtual const std::string getText() = 0;
    
};


/// A mock property that holds a string for testing.
/// \note This property does not expand on the TestInterface interface and so
/// does not impliment getInterfaces.  The default call to
/// TestInterface::getInterfaces will return the correct list.
class TestProperty : public TestInterface {

  public:

    TestProperty(std::string text = ""): text(text){}

    static const std::string name() { return "TestProperty"; }       
    virtual const std::string getName() const { return name(); }

    virtual const std::string getText() { 
        return text;
    }

  private:

    const std::string text;
    
};


/// A property that will never be instantiated.
/// The purpose of this property is to be a template argument for function
/// templates that take a Property type as an argument.  Those functions will
/// always fail because this property can not be instantiated.
class NoExistTestProperty : public Property {
  public:
    static const std::string name() { return "NoExistTestProperty"; }
};


/// A mock property that changes a string when its functions are called.
class StatusReporterTestProperty : public TestInterface {

  public:

    StatusReporterTestProperty() { text.push("constructed"); }

    static const std::string name() { return "TestProperty"; }       
    virtual const std::string getName() const { return name(); }

    virtual const std::string getText() {
        
        if (!text.size()) {
            return "";
        }
        
        std::string top = text.top();
        text.pop();
        return top;
    }

    virtual void initialize(const Node& owner) {
        if (text.top() != "initialized") {
            text.push("initialized");
        }
    }
    
    virtual void connect(const Node& newParent) {
        if (text.top() != "connected") {
            text.push("connected");
        }
    }
    
    virtual void disconnect(const Node& oldParent) {
        if (text.top() != "disconnected") {
            text.push("disconnected");
        }
    }
    
  private:

    std::stack<std::string> text;
    
};

#include "TaskManager.hpp"
//! A property that gives a task to a TaskManager.
class TestTaskMaker : public Property {

  public:

    TestTaskMaker() : executeCount(0) {}
      
    static const std::string name() { return "TestTaskMaker"; }       
    virtual const std::string getName() const { return name(); }
    virtual const InterfaceList getInterfaces() const { 
        InterfaceList interfaces = InterfaceList();
        interfaces.push_front(name());
        return interfaces;
    }
  
    virtual void connect(const Node& newParent) {
        manager = newParent.getProperty<TaskManager>();
        task = manager->addTask(TaskManager::TaskFunction(
            bind(&TestTaskMaker::execute, this, _1)));
    }
    
    virtual void disconnect(const Node& oldParent) {
        manager->removeTask(task);
    }
    
    bool haveTaskHandle() { return !task.empty(); }
    
    void execute(Scalar deltaTime) {
        ++executeCount;
        ++aggregateExecuteCount;
        manager->stop();
    }
    
    int executeCount;
    
    static int aggregateExecuteCount;
    
    TaskManager* manager;

  private:

    TaskManager::TaskHandle task;
    
};


#endif //_TESTPROPERTY_HPP_
