/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#ifndef PROPERTYFACTORYTESTS_HPP_
#define PROPERTYFACTORYTESTS_HPP_

#include <string>
#include <algorithm>
#include <boost/test/auto_unit_test.hpp>

#include "Property.hpp"

using namespace MidoriGraph;
using namespace std;


//! Check that the property requested from a PropertyFactory is correct.
/*!
PropertyFactory must return a property that is either exactly the requested
property, or impliments the requested property's interface.  This function
is a shortcut to ensure that the returned property is correct.

\param propertyName The interface/property name to be passed to the factory.
\param interfaceName The interface that must be implimented by the property.
\param attributes The property creation arguments to pass to the factory.
*/
template <class Factory>
void create(const string propertyName, const string interfaceName,
            const Property::AttributeMap& attributes =
                Property::AttributeMap()) {

    Factory factory;

    Property* property = factory.create(propertyName, attributes);
    BOOST_REQUIRE(property);
    
    BOOST_CHECK_EQUAL(*property->getInterfaces().begin(), interfaceName);
    
    Property::InterfaceList interfaces = property->getInterfaces();
    Property::InterfaceList::const_iterator begin = interfaces.begin();
    Property::InterfaceList::const_iterator end = interfaces.end();
   
    BOOST_CHECK_EQUAL(count(begin, end, interfaceName), 1);

}

#endif /*PROPERTYFACTORYTESTS_HPP_*/
