/* 
Copyright 2007, JD Marble <midorikid@yahoo.com>
 
This file is part of MidoriGraph.

MidoriGraph is free software; you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later
version.

MidoriGraph is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
MidoriGraph; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <boost/test/auto_unit_test.hpp>

#include "Node.hpp"
#include "LinearTaskManager.hpp"
#include "TestProperty.hpp"

using namespace MidoriGraph;


class TestFramework {
    
  public:
    TestFramework() {
        TestTaskMaker::aggregateExecuteCount = 0;
        
        root = Node::makeRoot();
        root.addProperty(new LinearTaskManager());
        
        child = root.createChild();
        child.addProperty(new TestTaskMaker());
        
        // This child ensures no run away TaskManager even if child is destroyed
        root.createChild().addProperty(new TestTaskMaker());
    }
    
    Node root;
    Node child;
};

int TestTaskMaker::aggregateExecuteCount;

//! Ensures that arbitrary destruction order doesn't mess things up.
BOOST_AUTO_TEST_CASE (LinearTaskManager_destructionOrder) {
    
    TestFramework* t = new TestFramework;
    BOOST_CHECK_NO_THROW(delete t);
}


BOOST_AUTO_TEST_CASE (LinearTaskManager_addTask) {
    
    TestFramework t;
    BOOST_CHECK(t.child.getProperty<TestTaskMaker>()->haveTaskHandle());   
}


BOOST_AUTO_TEST_CASE (LinearTaskManager_start) {
    
    TestFramework t;
    t.root.getProperty<TaskManager>()->start();
    
    BOOST_CHECK_EQUAL(t.child.getProperty<TestTaskMaker>()->executeCount, 1);   
}


BOOST_AUTO_TEST_CASE (LinearTaskManager_removeTask) {
    
    TestFramework t;
    t.child.remove();
    t.root.getProperty<TaskManager>()->start();
    
    // There are 2 TestTaskMaker tasks until child is removed, then there is 1.
    BOOST_CHECK_EQUAL(TestTaskMaker::aggregateExecuteCount, 1);   
}


BOOST_AUTO_TEST_CASE (LinearTaskManager_removeTask_badTaskHandle) {
    
    TestFramework t;
    
    // Attempt to remove a madeup task handle should throw.
    BOOST_CHECK_THROW(t.root.getProperty<TaskManager>()->removeTask(1337),
                      std::runtime_error);   
}
